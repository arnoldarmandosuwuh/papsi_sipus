<?php

include 'koneksi.php';

$id = $_GET['id'];

$sql = "SELECT * FROM buku WHERE id=$id";

$query = $conn->query($sql);

$result = array();
if ($query) {
	$list = array();
	while ($row = $query->fetch_assoc()) {
		$buku = array();
		$buku['id'] = $row['id'];
		$buku['kode_buku'] = $row['kode_buku'];
		$buku['judul_buku'] = $row['judul_buku'];
		$buku['penyusun'] = $row['penyusun'];
		$buku['tahun'] = $row['tahun'];
		$buku['status'] = $row['status'];
		$buku['cover'] = $row['cover'];
		array_push($list, $buku);
	}
	$result['status'] = 0;
	$result['messagge'] = "Success";
	$result['data'] = json_encode($list);
} else {
	$result['status'] = 1;
	$result['messagge'] = "0  result";
}
$conn->close();

echo json_encode($result);

?>