<?php

include 'koneksi.php';

$id = $_GET['id'];

$sql = "SELECT * FROM `peminjaman`,`buku` WHERE `buku`.`kode_buku` = `peminjaman`.`nomor_buku` and `peminjaman`.`id_pinjam`='$id'";

$query = $conn->query($sql);

$result = array();
if ($query) {
	$list = array();
	while ($row = $query->fetch_assoc()) {
		$pinjam = array();
		$pinjam['id'] = $row['id_pinjam'];
		$pinjam['kode_buku'] = $row['kode_buku'];
		$pinjam['judul_buku'] = $row['judul_buku'];
		$pinjam['penyusun'] = $row['penyusun'];
		$pinjam['tahun'] = $row['tahun'];
		$pinjam['status'] = $row['status_peminjaman'];
		$pinjam['cover'] = $row['cover'];
		$pinjam['tgl_pinjam'] = $row['tgl_pinjam'];
		$pinjam['tgl_kembali'] = $row['tgl_kembali'];
		array_push($list, $pinjam);
	}
	$result['status'] = 0;
	$result['messagge'] = "Success";
	$result['data'] = json_encode($list);
} else {
	$result['status'] = 1;
	$result['messagge'] = "0  result";
}
$conn->close();

echo json_encode($result);

?>