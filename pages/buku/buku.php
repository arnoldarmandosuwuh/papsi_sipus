        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Buku</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Data Buku
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <a href="index.php?page=pformbuku" class="btn btn-primary"><i class="fa fa-plus-square"></i> Tambah Data</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>Nomor</th>
                                        <th>Kode Buku</th>
                                        <th>Judul Buku</th>
                                        <th>Penyusun</th>
                                        <th>Tahun</th>
                                        <th>Status</th>
                                        <th>Cover</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $no=1; 
                                        include "../config/koneksi.php";
                                        $query = "select * from `buku`";
                                        $result = mysqli_query($conn,$query);
                                        while ($row=mysqli_fetch_array($result,MYSQLI_ASSOC)) {
                                            # code...
                                    ?>
                                    <tr class="odd gradeX">
                                        <td class="center"><?php echo $no; ?></td>
                                        <td class="center"><?php echo $row["kode_buku"]; ?></td>
                                        <td class="center"><?php echo $row["judul_buku"]; ?></td>
                                        <td class="center"><?php echo $row["penyusun"]; ?></td>
                                        <td class="center"><?php echo $row["tahun"]; ?></td>
                                        <td class="center"><?php echo $row["status"]; ?></td>
                                        <td class="center"><img width="80" height="100" src="<?php echo '../img/cover/'.$row["cover"]; ?>"/></td>
                                        <td class="center"><a href="<?php echo '?page=pformbuku&id='.$row['id']; ?>" class="btn btn-success"><i class="fa fa-pencil-square-o"></i> Edit</a> - <a href="<?php echo 'buku/buku_proses.php?act=delete&id='.$row['id']; ?>" class="btn btn-danger"><i class="fa fa-trash-o"></i> Delete</a></td>
                                    </tr>
                                    <?php
                                        $no++;
                                       }
                                    ?>
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->