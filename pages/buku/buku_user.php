        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Koleksi</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Buku Koleksi
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>Nomor</th>
                                        <th>Kode Buku</th>
                                        <th>Judul Buku</th>
                                        <th>Penyusun</th>
                                        <th>Tahun</th>
                                        <th>Status</th>
                                        <th>Cover</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $no=1; 
                                        include "../config/koneksi.php";
                                        $nrp = $_SESSION['username'];
                                            $sql = "select * from `peminjaman` where `nrp`='$nrp' and `status_peminjaman`='Belum Dikembalikan' or `status_peminjaman`='Proses'";
                                            $hasil = mysqli_query($conn,$sql);
                                            $count = mysqli_num_rows($hasil);
                                           
                                        $query = "select * from `buku` where `status`='Tersedia'";
                                        $result = mysqli_query($conn,$query);
                                        while ($row=mysqli_fetch_array($result,MYSQLI_ASSOC)) {
                                            # code...
                                    ?>
                                    <tr class="odd gradeX">
                                        <td class="center"><?php echo $no; ?></td>
                                        <td class="center"><?php echo $row["kode_buku"]; ?></td>
                                        <td class="center"><?php echo $row["judul_buku"]; ?></td>
                                        <td class="center"><?php echo $row["penyusun"]; ?></td>
                                        <td class="center"><?php echo $row["tahun"]; ?></td>
                                        <td class="center"><?php echo $row["status"]; ?></td>
                                        <td class="center"><img width="100" height="150" src="<?php echo '../img/cover/'.$row["cover"]; ?>"/></td>
                                        <?php 
                                            if ($count > 0){
                                                echo '<td class="center"><button href="buku/peminjaman_proses.php?id='.$row['kode_buku'].'" class="btn btn-success" disabled="true"><i class="fa fa-exchange"></i> Pinjam</button></td>';
                                            }
                                            else {
                                                echo '<td class="center"><a href="buku/peminjaman_proses.php?id='.$row['kode_buku'].'" class="btn btn-success"><i class="fa fa-exchange"></i> Pinjam</a></td>';
                                            }
                                        ?>
                                        
                                        
                                    </tr>
                                    <?php
                                        $no++;
                                       }
                                    ?>
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->