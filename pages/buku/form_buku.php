        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Buku</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Form Buku
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <?php 
                                        if (isset($_GET['id'])){
                                            include "../config/koneksi.php";
                                            $id = $_GET['id'];
                                            $query = "select * from `buku` where `id`=$id";
                                            $result = mysqli_query($conn,$query);
                                            while ($data = mysqli_fetch_array($result,MYSQLI_ASSOC)){
                                                $id = $data['id'];
                                                $kode_buku = $data['kode_buku'];
                                                $judul_buku = $data['judul_buku'];
                                                $penyusun = $data['penyusun'];
                                                $tahun = $data['tahun'];
                                                $cover = $data['cover'];
                                            }
                                        }
                                        else {
                                            $id = "";
                                            $kode_buku = "";
                                            $judul_buku = "";
                                            $penyusun = "";
                                            $tahun = "";
                                            $cover = "";
                                        }
                                    ?>
                                    <form role="form" action="buku/buku_proses.php?act=update" method="post" autocomplete="off" enctype="multipart/form-data" onsubmit="return postForm()">
                                        <input type="hidden" name="id" id="id" class="form-control" value="<?php echo $id; ?>" placeholder="Enter Your Name">
                                        <div class="form-group">
                                            <label>Kode Buku</label>
                                            <input class="form-control" name="kode" id="kode" value="<?php echo $kode_buku; ?>" placeholder="Masukkan Kode Buku"/>
                                        </div>
                                        <div class="form-group">
                                            <label>Judul Buku</label>
                                            <input class="form-control" name="judul" id="judul" value="<?php echo $judul_buku; ?>" placeholder="Masukkan Judul Buku"/>
                                        </div>
                                        <div class="form-group">
                                            <label>Penyusun</label>
                                            <input class="form-control" name="penyusun" id="penyusun" value="<?php echo $penyusun; ?>" placeholder="Masukkan Nama Penyusun"/>
                                        </div>
                                        <div class="form-group">
                                            <label>Tahun</label>
                                            <input class="form-control" name="tahun" id="tahun" value="<?php echo $tahun; ?>" placeholder="Masukkan Tahun Terbit"/>
                                        </div>
                                        <div class="form-group">
                                            <label>Cover</label>
                                            <input type="file" class="form-control" name="cover" id="cover" value="<?php echo $cover; ?>" accept="image/*"/>
                                        </div>
                                        <button type="submit" class="btn btn-primary"><i class="fa fa-check-circle"></i> Submit</button>
                                        <a href="?page=pbuku"><button type="reset" class="btn btn-danger"><i class="fa fa-times-circle"></i> Reset</button></a>
                                        <a href="?page=pbuku" class="btn btn-warning"><i class="fa fa-close"></i> Cancel</a>
                                    </form>
                                </div>

                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->
