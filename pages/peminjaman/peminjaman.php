        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Peminjaman</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Data Peminjaman
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <a href="index.php?page=pformpeminjaman" class="btn btn-primary"><i class="fa fa-plus-square"></i> Tambah Data</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>Nomor</th>
                                        <th>NRP</th>
                                        <th>Nama</th>
                                        <th>Kode Buku</th>
                                        <th>Judul</th>
                                        <th>Penulis</th>
                                        <th>Tanggal Pinjam</th>
                                        <th>Tanggal Kembali</th>
                                        <th>Status</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $no=1; 
                                        include "../config/koneksi.php";
                                        $query = "SELECT * FROM `peminjaman`,`buku`, `mahasiswa` WHERE `mahasiswa`.`NIM`=`peminjaman`.`nrp` and `buku`.`kode_buku` = `peminjaman`.`nomor_buku`";
                                        $result = mysqli_query($conn,$query);
                                        while ($row=mysqli_fetch_array($result,MYSQLI_ASSOC)) {
                                            if ($row['tgl_kembali']=='0000-00-00'){
                                                $tglKembali = '-';
                                            }
                                            else {
                                                $tglKembali = date("d M Y",strtotime($row['tgl_kembali']));
                                            }
                                    ?>
                                    <tr class="odd gradeX">
                                        <td><?php echo $no; ?></td>
                                        <td><?php echo $row["NIM"]; ?></td>
                                        <td><?php echo $row["NAMA"]; ?></td>
                                        <td><?php echo $row["kode_buku"]; ?></td>
                                        <td><?php echo $row["judul_buku"]; ?></td>
                                        <td><?php echo $row["penyusun"]; ?></td>
                                        <td><?php echo date("d M Y",strtotime($row['tgl_pinjam'])); ?></td>
                                        <td><?php echo $tglKembali; ?></td>
                                        <td><?php echo $row["status_peminjaman"]; ?></td>
                                        <?php if ($row['status_peminjaman']== 'Dikembalikan'){
                                            echo'<td class="text-center">
                                            <a href="peminjaman/peminjaman_proses.php?act=delete&id='.$row['id_pinjam'].'" class="btn btn-sm btn-danger"/><i class="fa fa-trash-o"></i> Delete</a>
                                            </td>';
                                        }
                                        else if ($row['status_peminjaman']== 'Proses'){
                                            echo'<td class="text-center">
                                            <a href="peminjaman/peminjaman_proses.php?act=pinjam&id='.$row['id_pinjam'].'" class="btn btn-sm btn-success"/><i class="fa fa-check-circle"></i> Pinjam</a> - 
                                            <a href="peminjaman/peminjaman_proses.php?act=batal&id='.$row['id_pinjam'].'" class="btn btn-sm btn-danger"/><i class="fa fa-ban"></i> Batalkan</a>
                                            </td>';
                                        }
                                        else if ($row['status_peminjaman']== 'Dibatalkan'){
                                            echo'<td class="text-center">
                                            <a href="peminjaman/peminjaman_proses.php?act=delete&id='.$row['id_pinjam'].'" class="btn btn-sm btn-danger"/><i class="fa fa-trash-o"></i> Delete</a>
                                            </td>';
                                        }
                                        else if ($row['status_peminjaman']== 'Belum Dikembalikan'){
                                            echo'<td class="text-center">
                                                <a href="peminjaman/peminjaman_proses.php?act=kembali&id='.$row['id_pinjam'].'" class="btn btn-sm btn-success"/><i class="fa fa-pencil-square-o"></i> Pengembalian</a> - 
                                                </td>';    
                                        }
                                        else {  
                                        echo'<td class="text-center">
                                            <a href="peminjaman/peminjaman_proses.php?act=kembali&id='.$row['id_pinjam'].'" class="btn btn-sm btn-success"/><i class="fa fa-pencil-square-o"></i> Pengembalian</a> - 
                                            <a href="peminjaman/peminjaman_proses.php?act=delete&id='.$row['id_pinjam'].'" class="btn btn-sm btn-danger"/><i class="fa fa-trash-o"></i> Delete</a>
                                            </td>';
                                        } ?>
                                    </tr>
                                    <?php
                                        $no++;
                                       }
                                    ?>
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->