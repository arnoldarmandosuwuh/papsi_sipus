<?php
session_start();
error_reporting(0);
include"../../config/koneksi.php";

$case = $_GET['act'];

switch ($case){
	case "insert":
		$id = $_POST['id'];
		$nrp = $_POST['nrp'];
		$kode_buku = $_POST['kode_buku'];
		$tgl_pinjam = $_POST['tgl_pinjam'];
		$tgl_kembali = $_POST['tgl_kembali'];

		if ($id==""){
			$sql = "UPDATE `buku` SET `status` = 'Tidak Tersedia' WHERE `kode_buku`='$kode_buku'";
			$hasil = mysqli_query($conn,$sql);
			$query = "INSERT INTO `peminjaman`(`nrp`, `nomor_buku`, `tgl_pinjam`, `status_peminjaman`) VALUES ('$nrp','$kode_buku','$tgl_pinjam', 'Belum Dikembalikan')";
		}
		else {
			$query = "UPDATE `peminjaman` SET `nrp`='$nrp',`nomor_buku`='$kode_buku',`tgl_pinjam`='$tgl_pinjam',`tgl_kembali`='$tgl_kembali' WHERE `id_pinjam`=$id";
		}
		$result=mysqli_query($conn,$query);
			if($result){
				echo "<script>
				alert('Insert Data Success');
				window.location='../index.php?page=ppeminjaman';
				</script>";
			}
			else {
				echo "<script>
				alert('Insert Data Failed');
				window.location='../index.php?page=ppeminjaman';
				</script>";
			}
	break;

	case "kembali":	
		$id = $_GET['id'];
		date_default_timezone_set('Asia/Jakarta');
		$tanggal= mktime(date("m"),date("d"),date("y"));
		$tglsekarang = date("Y-m-d", $tanggal); 
		
		$sql_kode = "SELECT * FROM `peminjaman` WHERE `id_pinjam`=$id";
		$res = mysqli_query($conn,$sql_kode);
		$data = mysqli_fetch_array($res,MYSQLI_ASSOC);
		$kode_buku = $data['nomor_buku'];
		$sql_buku = "UPDATE `buku` SET `status` = 'Tersedia' WHERE `kode_buku`='$kode_buku'";
		
		$res_buku = mysqli_query($conn,$sql_buku);
		$query = "UPDATE `peminjaman` SET `tgl_kembali` = '$tglsekarang', `status_peminjaman`='Dikembalikan' WHERE `id_pinjam`=$id";
		
		$result=mysqli_query($conn,$query);
		if($result){
			echo "<script>
			alert('Pengembalian Sukses');
			window.location='../index.php?page=ppeminjaman';
			</script>";
		}
		else {
			echo "<script>
			alert('Pengembalian Gagal');
			window.location='../index.php?page=ppeminjaman';
			</script>";
		}
	break;

	case "pinjam":	
		$id = $_GET['id'];

		$query = "UPDATE `peminjaman` SET `status_peminjaman`='Belum Dikembalikan' WHERE `id_pinjam`=$id";
		
		$result=mysqli_query($conn,$query);
		if($result){
			echo "<script>
			alert('Peminjaman Sukses');
			window.location='../index.php?page=ppeminjaman';
			</script>";
		}
		else {
			echo "<script>
			alert('Peminjaman Gagal');
			window.location='../index.php?page=ppeminjaman';
			</script>";
		}
	break;

	case "batal":	
		$id = $_GET['id'];
		date_default_timezone_set('Asia/Jakarta');
		$tanggal= mktime(date("m"),date("d"),date("y"));
		$tglsekarang = date("Y-m-d", $tanggal); 
		
		$sql_kode = "SELECT * FROM `peminjaman` WHERE `id_pinjam`=$id";
		$res = mysqli_query($conn,$sql_kode);
		$data = mysqli_fetch_array($res,MYSQLI_ASSOC);
		$kode_buku = $data['nomor_buku'];
		$sql_buku = "UPDATE `buku` SET `status` = 'Tersedia' WHERE `kode_buku`='$kode_buku'";
		
		$res_buku = mysqli_query($conn,$sql_buku);
		$query = "UPDATE `peminjaman` SET `tgl_kembali` = '$tglsekarang', `status_peminjaman`='Dibatalkan' WHERE `id_pinjam`=$id";
		
		$result=mysqli_query($conn,$query);
		if($result){
			echo "<script>
			alert('Dibatalkan');
			window.location='../index.php?page=ppeminjaman';
			</script>";
		}
		else {
			echo "<script>
			alert('Pembatalan Gagal');
			window.location='../index.php?page=ppeminjaman';
			</script>";
		}
	break;

	case "delete":
		$id = $_GET['id'];
		$sql = "DELETE FROM `peminjaman` WHERE `id_pinjam`=$id";
		
		$result = mysqli_query($conn, $sql);
		if ($result !== false){
			echo '<script>alert("Data telah terhapus");
			location="../index.php?page=ppeminjaman"</script>';
		}
	break;
}

