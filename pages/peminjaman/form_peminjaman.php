        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Peminjaman</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Form Peminjaman
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <?php 
                                        if (isset($_GET['id'])){
                                            include "../config/koneksi.php";
                                            $id = $_GET['id'];
                                            $query = "select * from `peminjaman` where `id_pinjam`=$id";
                                            $result = mysqli_query($conn,$query);
                                            while ($data = mysqli_fetch_array($result,MYSQLI_ASSOC)){
                                                $id = $data['id_pinjam'];
                                                $nrp = $data['nrp'];
                                                $nomor_buku = $data['nomor_buku'];
                                                $tgl_pinjam = $data['tgl_pinjam'];
                                                $tgl_kembali = $data['tgl_kembali'];
                                            }
                                        }
                                        else {
                                            $id = "";
                                            $nrp = "";
                                            $nomor_buku = "";
                                            $tgl_pinjam = "";
                                            $tgl_kembali = "";
                                        }
                                    ?>
                                    <form role="form" action="peminjaman/peminjaman_proses.php?act=insert" method="post" autocomplete="off">
                                        <input type="hidden" name="id" id="id" class="form-control" value="<?php echo $id; ?>" placeholder="Enter Your Name">
                                        <div class="form-group">
                                            <label>NRP</label>
                                            <input class="form-control" name="nrp" id="nrp" value="<?php echo $nrp; ?>" placeholder="Masukkan NRP Peminjam">
                                        </div>
                                        <div class="form-group">
                                            <label>Kode Buku</label>
                                            <input class="form-control" name="kode_buku" id="kode_buku" value="<?php echo $nomor_buku; ?>" placeholder="Masukkan Kode Buku">
                                        </div>
                                        <div class="form-group">
                                            <label>Tanggal Pinjam</label>
                                            <?php
                                                date_default_timezone_set('Asia/Jakarta');
                                                $tanggal= mktime(date("m"),date("d"),date("y"));
                                                $tglsekarang = date("Y-m-d", $tanggal);      
                                                
                                                if ($tgl_pinjam == ""){
                                                  echo  '<input type="text" class="form-control" name="tgl_pinjam" id="tgl_pinjam" value="'.$tglsekarang.'" placeholder="Masukkan Tanggal Peminjaman" readonly>';
                                                }
                                                else {
                                                    echo  '<input type="text" class="form-control" name="tgl_pinjam" id="tgl_pinjam" value="'.$tgl_pinjam.'" placeholder="Masukkan Tanggal Peminjaman" readonly>';
                                                }
                                            ?>
                                        </div>
                                        <div class="form-group">
                                            <label>Tanggal Kembali</label>
                                            <?php
                                                if ($tgl_kembali == ""){
                                                  echo  '<input type="text" class="form-control" name="tgl_kembali" id="tgl_kembali" value="" placeholder="Tanggal Pengembalian" readonly>';
                                                }
                                                else {
                                                    echo  '<input type="text" class="form-control" name="tgl_kembali" id="tgl_kembali" value="'.$tgl_kembali.'" placeholder="Tanggal Pengembalian" readonly>';
                                                }
                                            ?>
                                        </div>
                                        <button type="submit" class="btn btn-primary"><i class="fa fa-check-circle"></i> Submit</button>
                                        <a href="?page=user"><button type="reset" class="btn btn-danger"><i class="fa fa-times-circle"></i> Reset</button></a>
                                        <a href="?page=ppeminjaman" class="btn btn-warning"><i class="fa fa-close"></i> Cancel</button></a>
                                    </form>
                                </div>

                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->